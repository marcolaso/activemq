#!/usr/bin/python
# -*- coding: utf-8 -*-

#############################################################################
# purge ALL queue 
#
# todo : 
#    - purge une queue en parametre
#    - option all queue !
#
#############################################################################

#
# import lib
#
import re
import requests
from requests.auth import HTTPBasicAuth
import argparse
import time

#
# Variable globale
#
default_host="activemq.mondomain.fr"
default_port="8161"
default_user="admin"
default_password="admin"
regex = re.compile(r'[\n\r\t]')

#
# gestion des arguments
#
def args():
   parser = argparse.ArgumentParser()
   parser.add_argument("-H", "--host", help="hostname",    default=default_host)
   parser.add_argument("-P", "--port", help="port number", default=default_port)
   parser.add_argument("-u", "--user", help="user name",   default=default_user)
   parser.add_argument("-p", "--password", help="password",default=default_password)
   args = parser.parse_args()

   return(args.host,args.port,args.user,args.password)

#
# recup page http
#
def get_url_with_file(file_tmp,url):
   desc_write = open(file_tmp, "w")
   s = requests.Session()
   reponse=s.get(url, auth=HTTPBasicAuth(user,password))
   cookie=s.cookies
   desc_write.write(reponse.text)
   desc_write.close()

   return (cookie,s)

def get_url_with_cookie(url,cookie,s):
   JSESSIONID=str(cookie).split(' ')
   my_cookie = {
           'name': 'JSESSIONID',
           'value': JSESSIONID[2],
           'port': 'None',
           'port_specified': 'False',
           'domain': host,
           'domain_specified': 'False',
           'domain_initial_dot': 'False',
           'path':'/admin',
           'path_specified': 'True',
           'secure': 'False',
           'expires': 'None',
           'discard': 'True',
           'comment': 'None',
           'comment_url': 'None',
           'rfc2109': 'False'
   }

   # url = http://activemq.mondomain.fr.ch:8161/admin/purgeDestination.action;jsessionid=129wmko6lbhtu1oijj0kfuwbd1?JMSDestination=events&JMSDestinationType=queue&secret=7d0afcc9-ff87-453a-b05f-47419a5c82f1
   # url_purge = http://activemq.mondomain.fr:8161/admin/purgeDestination.action?JMSDestination=events&JMSDestinationType=queue&secret=7d0afcc9-ff87-453a-b05f-47419a5c82f1
   #
   url_purge=re.sub(';jsessionid=[a-zA-z0-9]*\?','?', url)
 
   reponse=s.get(url_purge, auth=HTTPBasicAuth(user,password), cookies=my_cookie)
   var=reponse.content.split('"')
   nb=0
   for i in var:
       if 'secret' in i:
          if 'purgeDestination' in i:
             break
   secret=i.split("=")[3]

   return(secret)

#
# traitement de la page http (fichier xml)
#
def traitement_html(cookie,s):
    secret=0
    desc_read = open(file_tmp, "r")
    for ligne in desc_read:
       if 'purgeDestination.action' in ligne:
          url="http://"+host+":"+port+"/admin/"+str(ligne.split('"')[1])
          # RECUP NEW secret pour eviter erreur activeMQ : Possible CSRF attack !!!
          if secret <> 0:
             url=url.split("secret")[0]+"secret="+secret
          secret=get_url_with_cookie(url,cookie,s)
    desc_read.close()

#
# MAIN
#
if __name__ == "__main__":
  (host,port,user,password)=args()

  url="http://"+host+":"+port+"/admin/queues.jsp"
  file_tmp="/tmp/.purge_activemq"+host+".xml"
  (cookie,s)=get_url_with_file(file_tmp,url)

  traitement_html(cookie,s)


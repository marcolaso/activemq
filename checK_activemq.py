#!/usr/bin/python3
# -*- coding: utf-8 -*-

#############################################################################
# Check activeMQ
#
# todo : 
#    - ajout un champ nombre max dans une queue dans la table activemq
#    - si http pas dispo sendemail erreur FATAL !
#    - Documentation !!!
#    - test si url = code 200
#
#############################################################################

#
# import lib
#
import re
import smtplib
from email.mime.text import MIMEText
from lxml import etree
import requests
from requests.auth import HTTPBasicAuth
import os.path
import argparse
import psycopg2

#
# Variable globale
#
default_email="ASI-GAD-Architecture@retraitespopulaires.ch"
default_host="jbsepr"
default_port="8161"
default_user="admin"
default_password="admin"
regex = re.compile(r'[\n\r\t]')
dic_err = {}
fromaddr="nagios@retraitespopulaires.ch"
subject=" - Check Queue : CRITICAL"
code_err=0
max_msg=500

# 
# connexion DB
#
connection = psycopg2.connect(user="u_db_nagios",
                                  password="dajfozqmskvtzaknwfis",
                                  host="slxpr501",
                                  port="5432",
                                  database="nagios")
cursor = connection.cursor()
#connection.autocommit = True

#
# gestion des arguments
#
def args():
   parser = argparse.ArgumentParser()
   parser.add_argument("-H", "--host", help="hostname",    default=default_host)
   parser.add_argument("-P", "--port", help="port number", default=default_port)
   parser.add_argument("-u", "--user", help="user name",   default=default_user)
   parser.add_argument("-p", "--password", help="password",default=default_password)
   args = parser.parse_args()

   return(args.host,args.port,args.user,args.password)

#
# creation dictionnaire a partir query DB
# but avoir cles   : queue
#           valeur : email
#
def f_email():
  dic_email = {}
  postgreSQL_select_Query = "select * from activemq"
  cursor.execute(postgreSQL_select_Query)
  records = cursor.fetchall()

  for row in records:
    queue=row[0]
    if len(row[1]) > 1:
       dic_email[queue]=row[1]
    else:
       dic_email[queue]=default_email

  return dic_email

#
# recup page http
#
def get_url(file_tmp,url):
   desc_write = open(file_tmp, "w")
   reponse=requests.get(url, auth=HTTPBasicAuth(user,password))
   desc_write.write(reponse.text)
   desc_write.close()

#
# creation dictionnaire 
#     cles : email
#     liste des queues en erreur
#
def f_err(queue_name,queue_size,email):
   #print ("DEBUG f_err : "+queue_name+" "+email)
   global dic_err
   t_email = email.split(' ')
   if (( "error" in queue_name or int(queue_size) > max_msg ) or ( "dlq" in queue_name or int(queue_size) > max_msg )):
     for email in t_email:
       #print ("DEBUG email dans t_email : "+email)
       if '@' in  email:
         if email in dic_err:
           dic_err.get(email).append(queue_name+" : "+queue_size+" message(s)")
         else:
           dic_err[email]=[queue_name+" : "+queue_size+" message(s)"]
   lock_file="/tmp/.check_activeMQ_"+host+"_"+queue_name+".lock"
   desc_lock=open(lock_file,"w")
   desc_lock.write(queue_size)
   desc_lock.close()

#
# envoyer email
#
def send_email(email,msg):
   global code_err
   txt=""
   for i in msg:
     txt=txt+"\r\n"+i
   data = ("From: %s\r\nTo: %s\r\nSubject: %s %s \r\n%s" % (fromaddr, email ,host, subject, str(txt)))
   #print("DEBUG data : "+data)
   server = smtplib.SMTP('localhost')
   #server.set_debuglevel(1)
   server.sendmail(fromaddr, email, data)
   server.quit()
   code_err=2

#
# update DB
#
def update_db(queue_name):
   postgreSQL_insert_Query = "insert into activemq values ('"+queue_name+"','') ; "
   try:
     cursor.execute(postgreSQL_insert_Query)
     connection.commit()
     #print ("DEBUG " +postgreSQL_insert_Query) 
   except:
     print ("Cannot insert")

#
# traitement de la page http (fichier xml)
#
def traitement_xml():
   tree = etree.parse(file_tmp)
   root = tree.getroot()

   for queue in root.iter('queue'):
      queue_name=str(queue.get('name')).lower()
      for size in queue.iter('stats'):
         queue_size=size.get('size')
         lock_file="/tmp/.check_activeMQ_"+host+"_"+queue_name+".lock"
         if int(queue_size) > 0:
            try:
               email=dic_email[queue_name]
            except KeyError:
               email=default_email
               update_db(queue_name)
            if not os.path.exists(lock_file):
               f_err(queue_name,queue_size,email)
            else:
               desc_lock=open(lock_file,"r")
               nb=regex.sub("", desc_lock.read())
               desc_lock.close()
               if int(queue_size) > int(nb):
                  f_err(queue_name,queue_size,email)
         else:
            if os.path.exists(lock_file):
              os.remove(lock_file)

if __name__ == "__main__":
  (host,port,user,password)=args()
  url="http://"+host+".lesrp.ch:"+port+"/admin/xml/queues.jsp"
  file_tmp="/tmp/.wget_activemq_subscribers_"+host+".xml"
  dic_email=f_email()
  get_url(file_tmp,url)
  traitement_xml()
  for key, value in dic_err.items():
    send_email(key,value)
  cursor.close ()
  exit(int(code_err))
